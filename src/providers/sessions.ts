import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import PouchDB from 'pouchdb'

/*
  Generated class for the Sessions provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/

@Injectable()
export class Sessions {
  data: any;
  db: any;
  remote: any; 
  change: any; 

  constructor(public http: Http) {
    this.db = new PouchDB('ptoms');
    this.remote = 'http://localhost:5984/ptoms/';
    let options = {
      live: true,
      retry: true,
      continuous: true
    };
    this.db.sync(this.remote, options);
  }


getSessions() {
    if (this.data) {
    return Promise.resolve(this.data);
  }
 
  return new Promise(resolve => {
    this.db.allDocs({
      include_docs: true
    }).then((result) => {
      this.data = [];
      let docs = result.rows.map((row) => {
        this.data.push(row.doc);
      });
      resolve(this.data);
      this.db.changes({live: true, since: 'now', include_docs: true}).on('change', (change) => {
      this.handleChange(change);
      });
    }).catch((error) => {
      console.log(error);
    }); 
  });
}

createSession(session){
    this.db.post(session);
  }
  updateSession(session){
    this.db.put(session).catch((err)=>{
      console.log(err);
    });
  }
  deleteSession(session){
    this.db.remove(session).catch((err)=> {
      console.log(err);
    });
  }
  handleChange(change){
  let changedDoc = null;
  let changedIndex = null;
 
  this.data.forEach((doc, index) => {
    if(doc._id === change.id){
      changedDoc = doc;
      changedIndex = index;
    }
  });
  //A document was deleted
  if(change.deleted){
    this.data.splice(changedIndex, 1);
      } 
  else {
    //A document was updated
    if(changedDoc){
      this.data[changedIndex] = change.doc;
      } 
    //A document was added
    else {
      this.data.push(change.doc); 
      }
    }
  }


}