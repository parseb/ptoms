import { Component } from '@angular/core';

import { NavController, AlertController } from 'ionic-angular';
import { Sessions } from '../../providers/sessions'; 

import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
   
})
export class HomePage {
  //sessions: any; 
      viewjoin:boolean;
      viewcreate:boolean; 
      title = this.title;
      type = this.type;
      sessionForm: FormGroup;

  constructor(public navCtrl: NavController, public sessionService: Sessions, public alertCtrl: AlertController,public fb: FormBuilder) {
    this.sessionForm = fb.group({
          'title' : [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(46)])],
         // 'password': [null, Validators.compose([Validators.required, Validators.minLength(8), checkFirstCharacterValidator(/^\d/i)])],
            'type' : ''
})} 

     // sessionid = this.sessionid; 
     // sessionpassword = this.sessionpassword; 
       
  // if this.activesession {NavController.push(SessionPage(session))}
/*
  ionViewDidLoad(){
     // this.sessionService.getSessions().then((data) => {
     // this.sessions = data;
    });
  }

  createSession(){
    let prompt = this.alertCtrl.create({
      title: 'Add',
      message: 'Add Session:',
      inputs: [
        {
          name: 'title'
        }
      ],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Save',
          handler: data => {
            this.sessionService.createSession({title: data.title});
          }
        }
      ]
    });
 
    prompt.present();
  }

  deleteSession(session){
    this.sessionService.deleteSession(session);
  }
*/ 


//Toggle Create/Join
    


  setFormCreateVisible = function(){ 
        this.viewcreate = true;
        this.viewjoin = false;  
      // return(this.viewcreate);
    }
    setFormJoinVisible = function() {
        this.viewjoin = true; 
        this.viewcreate = false;
        //return(this.viewjoin); 
    }
    submitForm(value: any):void{
        console.log('Form submited!')
        this.sessionService.createSession({title: value.title,
                                            type: value.type    
                                          });
        console.log(value);
        navCtrl.
    }
}


